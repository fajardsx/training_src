import {createStackNavigator,createAppContainer} from 'react-navigation';
import drawerroute from './drawer'
const AppNavigator = createStackNavigator({
    Drawer: drawerroute
  },
  {
      initialRouteName:"Drawer",
      headerMode:'none'
  }
);

export default createAppContainer(AppNavigator);