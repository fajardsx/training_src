
import React from 'react'
import {createDrawerNavigator,createStackNavigator} from 'react-navigation'
import HomeScreen from './app/screens/Home_Screen';
import ProductDetailScreen from './app/screens/products/ProductDetailsScreen';
import ProductScreen from './app/screens/products/ProductScreen';
import DrawerScreen from './DrawerScreen';
import SimplereduxScreen from './app/screens/SimpleRedux';
import Cart_Screen from './app/screens/Cart_Screen';

const ProductStack = createStackNavigator({
    Home: HomeScreen,
    Product: ProductScreen,
    ProductDetail:ProductDetailScreen,
    SimpleRedux:SimplereduxScreen,
    Cart:Cart_Screen
},{
        headerMode: 'none'
})

const DrawerContainer = createDrawerNavigator({
    Home:ProductStack
},{
    initialRouteName:"Home",
    contentComponent:DrawerScreen,
  
}   
);

export default DrawerContainer