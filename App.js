/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */



/*
  import {Platform, StyleSheet, Text, View} from 'react-native';
const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});


export default class App extends Component {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
});*/
import React, { Component } from 'react';

//NAVIGAATION
import { createAppContainer } from 'react-navigation';
import AppNavigation from "./AppNavigator"
const AppContainer = createAppContainer(AppNavigation)

//REDUX 
//import { createStore } from "redux";
//import reducer from "./app/redux/reducers/indexReducer";
//const store= createStore(reducer);

import { Provider } from "react-redux";

//IMport REDUX - PRESIST
//REDUX-SAGA 
//REDUX-TUN
import { PersistGate } from 'redux-persist/integration/react';
import configureStore from './app/redux/configureStore';


const {store,presistor} = configureStore();
export default class App extends Component {
  render() {
    return (
      <Provider store={store}>
        {
          // add to 
        }
        <PersistGate loading={null} persistor={presistor}>
          <AppContainer />
        </PersistGate>
      </Provider>
    );
  }
}