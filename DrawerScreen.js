
import React from 'react'
import { View, } from "react-native";
import { SafeAreaView } from 'react-navigation';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Text,
    Button,
    List,
    ListItem,
    Icon, Left, Body, Right, Title, Badge
} from 'native-base';
import { connect } from 'react-redux'; //connect class with redux

class DrawerScreen extends React.Component {
    
    render() {
        return <Container>
                   <Content>
                       <View>
                           {this.renderHeader()}
                           
                       </View>
                       <ListItem onPress={()=>this.props.navigation.navigate('Home')}>
                           <Body>
                               <View>
                                   <Text>Home</Text>
                                   
                               </View>
                           </Body>
                       </ListItem>
                       <ListItem onPress={()=>this.props.navigation.navigate('Cart')}>
                           <Body>
                               <View style={{flexDirection: 'row'}}>
                                   <Text>Cart</Text>
                                    <Badge>
                                        <Text>{this.props.count}</Text>
                                    </Badge>
                               </View>
                                
                           </Body>
                       </ListItem>
                       <ListItem onPress={()=>this.props.navigation.navigate('SimpleRedux')}>
                           <Body>
                               <View>
                                   <Text>Simple Redux</Text>
                               </View>
                           </Body>
                       </ListItem>
                    </Content>
        </Container>
    }

    renderHeader() {
        return (
            <View style={{ paddingLeft: 20 }}>
                <View style={{ flexDirection: "row", marginTop: 50, marginBottom: 20 }}>
                    <Thumbnail
                        style={{ width: 72, height: 72, borderRadius: 35 }}
                        source={require('./assets/images/profil.jpg')}
                    />

                    <View
                        style={{
                            flexDirection: "column",
                            flex: 1,
                            marginLeft: 10,
                        }}>
                        <Text
                            style={{
                                color: '#333',
                                fontSize: 14,
                                fontWeight: "900",
                            }}>
                            Keanu reeves
                            </Text>

                        <View style={{ flexDirection: "row" }}>
                            <Text
                                style={{
                                    marginLeft: 4,
                                    color: '#333',
                                    fontSize: 10,
                                }}>
                                100 Point
                                </Text>
                        </View>
                        <Button
                            small
                            bordered
                            style={{
                                borderColor: '#ffffff',
                                borderRadius: 8,
                                marginTop: 10,
                            }}>
                            <Text
                                style={{
                                    alignContent: "center",
                                    color: '#ffffff',
                                    fontSize: 10,
                                }}>
                                Edit Profile
                                </Text>
                        </Button>
                    </View>
                </View>
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        count: Object.values(state.products).reduce((accumulator, product) => accumulator += product.total, 0)
    }
}
export default connect(mapStateToProps)(DrawerScreen);