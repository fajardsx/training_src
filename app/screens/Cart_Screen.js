import React from 'react'
import {
    View,
    Platform,
    StyleSheet,
    Image,
    FlatList
} from "react-native";
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Badge,
    Text,
    Button,
    Icon, Left, Body, Right, Title
} from 'native-base';

import { connect } from 'react-redux'; //connect class with redux




class CartScreen extends React.Component{

    render(){
      
        return(<Container>
            <Header>
                <Left>
                    <Button
                        transparent
                        onPress={() => this.props.navigation.toggleDrawer()}>
                        <Icon name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>Cart</Title>
                </Body>
                <Right>
                    <Badge>
                        <Text style={{ color: '#fff' }}>{this.props.count}</Text>
                    </Badge>

                </Right>
            </Header>
            <Content>
                <FlatList
                    data={this.props.products}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                        <View>
                            <Card>
                                <CardItem>
                                    <Left>
                                        <Body>
                                            <Text>{item.product.name}</Text>
                                            <Text note size={18} color={'#ff0000'}>{item.product.category}</Text>
                                        </Body>
                                    </Left>
                                    <Right>
                                        <Button transparent
                                            onPress={()=>this.removeCart(item.product.id)}
                                        >
                                            <Icon name="trash" />
                                        </Button>
                                    </Right>
                                </CardItem>
                                <CardItem cardBody>
                                    <Image source={require('../../assets/products/screen/image_kaleng.jpg')}
                                        style={{ height: 200, width: null, flex: 1 }}
                                        resizeMode="contain" />

                                </CardItem>
                                <CardItem>
                                    <Text>{item.product.description}</Text>
                                </CardItem>
                                <CardItem>
                                    <Left>
                                        <Button transparent>
                                            <Icon active name="pricetag" />
                                            <Text>Rp. {item.product.regular_price}</Text>
                                        </Button>
                                    </Left>
                                    <Right>
                                        <View style={{ flexDirection: "row" }}>
                                            <Button
                                                onPress={() => this.incrementCart(item.product.id)}
                                                transparent
                                                style={{
                                                    borderWidth: 1,
                                                    borderColor: '#d6d6d6',
                                                    padding: 4,
                                                    borderRadius: 8,
                                                }}>
                                                <Icon name="add" size={14} color={'#fff'} />
                                            </Button>
                                            <View style={{ marginLeft: 4, marginRight: 4 }} />
                                            <Text
                                                style={{
                                                    alignSelf: "center",
                                                }}>
                                                {item.total}
                                            </Text>
                                            <View style={{ marginLeft: 4, marginRight: 4 }} />
                                            <Button
                                                transparent
                                                onPress={() => this.decrementCart(item.product.id)}
                                                style={{
                                                    borderWidth: 1,
                                                    borderColor: '#d6d6d6',
                                                    padding: 4,
                                                    borderRadius: 8,
                                                }}>
                                                <Icon name="remove" size={14} color={'#fff'} />
                                            </Button>
                                        </View>
                                    </Right>
                                </CardItem>
                            </Card>
                        </View>
                    )}
                />
            </Content>
        </Container>
        );
    }
    //create function call get redux state
    removeCart = (id) =>{
        this.props.removeCart(id);
    }
    incrementCart = (id) =>{
        this.props.incrementTotalOrder(id);
        console.log("RAMBAH",id)
    }
    decrementCart = (id) =>{
        this.props.decrementTotalOrder(id)
    }
}
//1 attach redux state to props
const mapStateToProps = (state) => {
    //IMPORTANt Object.values(OBJECT) needed if redux object
    return {
        count: Object.values(state.products).reduce((accumulator, product) => accumulator += product.total, 0),
        products:Object.values(state.products)
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
       removeCart:function(id){
           dispatch({type:"DELETE_CART",id:id})
       },
       incrementTotalOrder:function(id){
           dispatch({type:"UPDATE_CART",id:id,method:"PLUS"})
       },
        decrementTotalOrder:(id)=>{
           dispatch({type:"UPDATE_CART",id:id,method:"MINUS"})
       }
    }
}

//2 attach redux to class
export default connect(mapStateToProps, mapDispatchToProps)(CartScreen);