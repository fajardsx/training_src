import React from 'react'
import {
    View,
    Platform,
    StyleSheet,
    Image,
    FlatList
} from "react-native";
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Thumbnail,
    Text,
    Button,
    Icon, Left, Body, Right, Title
} from 'native-base';
//COMMUNICATION REDUX
import { increment,decerement } from "../redux/actions/indexActions"; //redux action
import { bindActionCreators } from "redux"; //call redux actions
import { connect } from 'react-redux'; //connect class with redux

class SimpleRedux extends React.Component {
    render() {
        console.log("totalProps ",this.props.count)
        return (
            <Container>
                <Header>
                    <Left>
                        <Button
                            transparent
                            onPress={() => this.props.navigation.toggleDrawer()}>
                            <Icon name="arrow-back" />
                        </Button>
                    </Left>
                    <Body>
                        <Title>Redux Counter</Title>
                    </Body>
                    <Right>
                        <Text style={{color:"#fff"}}>{this.props.count}</Text>
                    </Right>
                </Header>
                <Content padder>
                    <Card>
                        <CardItem>
                            <Text style={{ fontSize: 20, fontWeight: 'bold' }}>
                            {this.props.count}
                            </Text>
                        </CardItem>
                    </Card>
                    <Button dark bordered 
                        onPress={()=>this.props.increment()}
                    >
                        <Text>Increment</Text>
                    </Button>
                    <Button dark bordered
                        onPress={() => this.props.decrement()}
                    >
                        <Text>Decrement</Text>
                    </Button>
                </Content>
            </Container>
        );
    }
}
//redux 
// buat ungsi mapping state dan dispatch
//set state
function mapStateToProps(state) {
    return {
        count: state.totalOrder
    }
}
//dispatch state
function mapDispatchToProps(dispatch) {
    return bindActionCreators(
        {
            increment:increment,
            decrement:decerement
        },
        dispatch
    )
}
//wrap class dengan fungsi connect
export default connect(mapStateToProps,mapDispatchToProps)(SimpleRedux);