
import React from 'react'
import {
    View,
    Platform,
    StyleSheet,
    Image,
    FlatList,
    ActivityIndicator,
    AsyncStorage
} from "react-native";
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Badge,
    Text,
    Button,
    Icon, Left, Body, Right, Title
} from 'native-base';

import { connect } from 'react-redux'; //connect class with redux
import firebase from 'react-native-firebase'; //FCM IMPORT
var products = require("../sources/products.json");
//REALM
import ProductProvider from "../models/ProductProvider";
import Products from "../models/Products";
//

class HomeScreen extends React.Component {
    constructor(props) {
        super(props);
        this.state={
            isLoading:true,
            products: products
        }
    }

    componentDidMount(){
        this.getLocalProducts();
        this.fetchProduct();
        this.notificationBuilder();
        this.checkFcmPermission();

        
    }
    //FCM TRAINING--------------------------------------------------------------------------------------
    async getToken(){
        const fcmToken = await AsyncStorage.getItem("fcmToken");
        console.log("Home js : exitting token > "+fcmToken);

        if(!fcmToken){
            firebase.messaging().getToken().then(fcmToken=>{
                console.log("Home .js : new token > " + fcmToken);
                AsyncStorage.setItem("fcmToken",fcmToken)
            });
        }else{
            this.onTokenFreshListener = firebase.messaging().onTokenRefresh(fcmToken=>{
                console.log("Home .js : refresh token > " + fcmToken);
                AsyncStorage.setItem("fcmToken", fcmToken)
            })
        }
    }

    async checkFcmPermission(){
        const enable = await firebase.messaging().hasPermission();
        if(enable){
            this.getToken()
        }else{
            this.requestNotificationPermission();
        }
    }

    async requestNotificationPermission(){
        try{
            await firebase.messaging().requestPermission();
            this.getToken();
        }catch(error){
            console.log("Permission Rejected");
        }
    }
    //--
    //Creating notification builder
    notificationBuilder = () => {
        // Creating channel for android 8.0.0 (API 26)
        const channel = new firebase.notifications.Android.Channel(
            "channelId",
            "channelId",
            firebase.notifications.Android.Importance.Max
        ).setDescription("Channel Description");
        firebase.notifications().android.createChannel(channel);
        firebase.messaging().subscribeToTopic("channel_topic");

        // the listener returns a function you can use to unsubscribe
        this.unsubscribeFromNotificationListener = firebase
            .notifications()
            .onNotification(notification => {
                // for Android Notification
                if (Platform.OS === "android") {
                    const localNotification = new firebase.notifications.Notification(
                        {
                            sound: "default",
                            show_in_foreground: true,
                        }
                    )
                        .setNotificationId(notification.notificationId)
                        .setTitle(notification.title)
                        .setSubtitle(notification.subtitle)
                        .setBody(notification.body)
                        .setData(notification.data)
                        .android.setChannelId("channelId") // e.g. the id you chose above
                        .android.setSmallIcon("ic_stat_ic_notification") // set icon (must be samee with manifest)
                        .android.setColor("#2196F3") // you can set a color here (hexcode must be same with manifest)
                        .android.setAutoCancel(true)
                        .android.setBigText(notification.body)
                        .android.setPriority(
                            firebase.notifications.Android.Priority.Default
                        );

                    //handle notification action if application is on foreground / background
                    firebase
                        .notifications()
                        .onNotificationOpened(this.onNotificationPressed);

                    // Displaying notification
                    firebase
                        .notifications()
                        .displayNotification(localNotification)
                        .catch(error =>
                            console.error(
                                "Home.js : " +
                                "firebase.notifications Android error : " +
                                error
                            )
                        );
                } else if (Platform.OS === "ios") {
                    const localNotification = new firebase.notifications.Notification()
                        .setNotificationId(notification.notificationId)
                        .setTitle(notification.title)
                        .setSubtitle(notification.subtitle)
                        .setBody(notification.body)
                        .setData(notification.data)
                        .ios.setBadge(notification.ios.badge);

                    firebase
                        .notifications()
                        .onNotificationOpened(this.onNotificationPressed);
                    firebase
                        .notifications()
                        .displayNotification(localNotification)
                        .catch(error =>
                            console.error(
                                "Home.js : " +
                                "firebase.notifications IOS error : " +
                                error
                            )
                        );
                }
            });
        this.handleInitialNotification();
    };

    //Handling initialNotification (opened notification when apps closed)
    async handleInitialNotification() {
        const oldNotification = await AsyncStorage.getItem("notification");
        console.log("Home.js : " + "oldNotifID -> " + oldNotification);

        //handle notification action If application is closed
        firebase
            .notifications()
            .getInitialNotification()
            .then(notificationOpen => {
                //action here
            })
            .catch(error =>
                console.log(
                    "Home.js : " +
                    "firebase.getInitialNotification error : " +
                    error
                )
            );
    }

    //function for handling opened notification on foreground / background (not killed / closed)
    onNotificationPressed = notificationOpen => {
        if (notificationOpen) {
            // action here
        }
    };

    //--------------------------------------------------------------------------------------
    getLocalProducts(){
        let products = ProductProvider.findAll();

        if(Object.keys(products).length != 0){
            let dataProducts = [...products];
            this.setState({
                products:dataProducts
            })
            //console.log("local",dataProducts)
        }
        this.setState({isLoading:false});
    }
    async fetchProduct(){
        this.setState({isLoading:true});
        try{
            console.log("fetch")
            let response = await fetch('http://192.168.100.61:8080/api/products');
            //Kalo pake header
            /*
                  let response = await fetch('http://192.168.100.61:8080/api/products',
                    header:{
                        
                    }
                  );
            */
           let responsiveJson = await response.json();
           console.log(responsiveJson)
           this.setState({
               isLoading:false,
               //products:responsiveJson
           },()=>{
               if(responsiveJson.length>0){
                    //ProductProvider.deleteAll();
                    responsiveJson.map(function(item){
                        let product = new Products(item.id, item.sku, item.name, item.category,item.description,item.regular_price,item.discount_price,item.quantity);
                        ProductProvider.save(product);
                    })
                  this.getLocalProducts();
               }
           });
        }catch(error){
            console.log(error);
        }
    }
    
    render() {
        if(this.state.isLoading == true){
           // return <ActivityIndicator/>
        }
        return <Container>
            <Header>
                <Left>
                    <Button
                        transparent
                        onPress={() => this.props.navigation.toggleDrawer()}>
                        <Icon name="menu" />
                    </Button>
                </Left>
                <Body>
                    <Title>HomeScreen</Title>
                </Body>
                <Right>
                    <Badge>
                        <Text style={{ color: '#fff' }}>{this.props.count}</Text>
                    </Badge>
                   
                </Right>
            </Header>
            <Content>
                <FlatList
                    data={this.state.products}
                    keyExtractor={(item, index) => index.toString()}
                    renderItem={({ item }) => (
                        <View>
                            <Card>
                                <CardItem>
                                    <Left>
                                        <Body>
                                            <Text>{item.name}</Text>
                                            <Text note>{item.category}</Text>
                                        </Body>
                                    </Left>
                                </CardItem>
                                <CardItem cardBody>
                                    <Image source={require('../../assets/products/screen/image_kaleng.jpg')}
                                        style={{ height: 200, width: null, flex: 1 }}
                                        resizeMode="contain" />

                                </CardItem>
                                <CardItem>
                                    <Text>{item.description}</Text>
                                </CardItem>
                                <CardItem>
                                    <Left>
                                        <Button transparent>
                                            <Icon active name="pricetag" />
                                            <Text>Rp. {item.regular_price}</Text>
                                        </Button>
                                    </Left>
                                    <Right>
                                        <Button onPress={()=>this.addToCart(item)}>
                                            <Icon active name="cart" />
                                            <Text>Add To Cart</Text>
                                        </Button>
                                    </Right>
                                </CardItem>
                            </Card>
                        </View>
                    )}
                />
               
            </Content>

        </Container>
    }

    addToCart=(product)=>{
        this.props.addToCart(product);
    }
}
const mapStateToProps = (state) =>{
    return {
        count: Object.values(state.products).reduce((accumulator, product) => accumulator += product.total,0)
    }
}
const mapDispatchToProps = (dispatch) =>{
    return {
        addToCart:(product)=>{
            dispatch({ type: "ADD_CART", id: product.id, payload: { product: product,total:1}})
        }
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen)