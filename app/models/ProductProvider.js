import realm from './realm';

const PRODUCTS_SCHEMA = "Products";
const ProductProvider = {
    //FUNGSI-FUNGSI DB MYSQL:insert,update,delete,dll
    save:function(product){
        realm.write(()=>{
            console.log("ProductProvider",product)
            realm.create(PRODUCTS_SCHEMA,product,true);
        })
    },
    findAll:function(){
        return realm.objects(PRODUCTS_SCHEMA);
    },
   
    find: function (id) {
        return realm.objectForPrimaryKey(PRODUCTS_SCHEMA, id);
    },
    update: function (id, product) {
        realm.create(PRODUCTS_SCHEMA, product, true);
    },
    delete: function (product) {
        realm.write(() => {
            realm.delete(product);
        });
    },
    deleteAll: function () {
        realm.write(() => {
            realm.delete(ProductProvider.findAll());
        });
    }

}

export default ProductProvider;