class Products{
    constructor(id,sku,name,category,description,regular_price,discount_price,quantity){
        this.id = id;
        this.sku = sku;
        this.name = name;
        this.category = category;
        this.description = description;
        this.regular_price = regular_price;
        this.discount_price = discount_price;
        this.quantity = quantity;
    }
}

module.exports = Products;