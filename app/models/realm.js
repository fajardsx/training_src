const Realm = require('realm'); //1. import realm 

//TABLE in SQL
//2. import realm
const ProductSchema={
    name:"Products",
    primaryKey:"id",
    properties:{
        id:"int",
        sku:"string",
        name:"string",
        category:"string",
        description:"string",
        regular_price:"int",
        discount_price:"int",
        quantity:"int"
    }
}

export default new Realm({
    schema:[ProductSchema]
})