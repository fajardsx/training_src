// 1. import lib

import {createStore} from 'redux';
import {persistStore,persistReducer} from 'redux-persist'

import { AsyncStorage } from "react-native";

import reducer from './reducers/indexReducer';


//2. CONFIG
const presistConfig = {
    key:'root',
    storage:AsyncStorage
}

//
const presistReducer = persistReducer(presistConfig,reducer);

//3. export 
export default ()=>{
    let store = createStore(presistReducer);
    let presistor = persistStore(store);
    return {store,presistor}
}
