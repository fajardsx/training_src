const initalState={
    totalOrder:10,
    products:{

    }
}

const reducer = (state=initalState,action)=>{
    if (action.type === "ADD_CART") {
        const similaItems = Object.values(state.products).filter((item) => item.product.id === action.id);

        if (similaItems.length > 0) {
            let tempProduct = state.products[action.id];
            tempProduct.total += 1;
            state.products[action.id] = tempProduct;
            return {
                ...state,//copy prev state
                products: {
                    ...state.products,
                }
            }
        } else {
            return {
                ...state,
                products: {
                    ...state.products,
                    [action.id]: action.payload
                }
            }
        }

    } if (action.type === "DELETE_CART") {
        delete state.products[action.id]
        return {
            ...state, products: {
                ...state.products,
            }
        }
    } if (action.type === "UPDATE_CART") {
        const similaItems = Object.keys(state.products).filter((item) => item == action.id);

        if (similaItems.length > 0) {
            let tempProduct = state.products[action.id];
            if (action.method === "PLUS") {
                tempProduct.total += 1;
            } if (action.method === "MINUS") {
                if (tempProduct.total > 1) {
                    tempProduct.total -= 1;
                }
            }
            state.products[action.id] = tempProduct;

        }
        return {
            ...state,
            products: {
                ...state.products,
            }
        }
    }
    return state;
}

export default reducer;