const initalState={
    totalOrder:10,
    products:{
        
    }
}

const reducer = (state=initalState,action)=>{
    if(action.type === "INCREMENT"){
        return{
            ...state,
            totalOrder:state.totalOrder+1
        }
    }

    if(action.type === "DECREMENT"){
        let tempOrder = 0;
        if(state.totalOrder>0){
            tempOrder = state.totalOrder - 1
        }

        return{
            ...state,
            totalOrder: tempOrder
        }
    }
    return state;
}

export default reducer;