const redux = require('redux');
const createStore = redux.createStore;

//REDUX STATE
const initialState={
    value:0,
    age:17,
    products:{
    }
}

//2. REDUCER
const reducer = (state = initialState,action) =>{
    if(action.type === "ADD_AGE"){
        return {
            ...state,
            age:state.age+1
        }
    }
    if(action.type==="SET_VALUE"){
        return{
            ...state,
            value:state.value+action.setValue
        }
    }
    if (action.type === "ADD_CART") {
        const similaItems = Object.values(state.products).filter((item)=>item.product.id === action.id);

        if(similaItems.length>0){
            let tempProduct = state.products[action.id];
            tempProduct.total+=1;
            state.products[action.id]=tempProduct;
            return {
                ...state,
            }
        }else{
            return {
                ...state,
                products: {
                    ...state.products,
                    [action.id]: action.payload
                }
            }
        }
       
    } if (action.type === "UPDATE_CART") {
        const similaItems = Object.keys(state.products).filter((item) => item === action.id);

        if (similaItems.length > 0) {
            let tempProduct = state.products[action.id];
            if(action.method === "PLUS"){
                tempProduct.total += 1;
            } if (action.method === "MINUS"){
                if (tempProduct.total>1){
                    tempProduct.total -= 1;
                }
            }
            state.products[action.id] = tempProduct;
           
        }
        return {
            ...state,
        }
    }if (action.type === "DELETE_CART") {
        delete state.products[action.id]
        return {
            ...state,
        }
    } 
    return state;
}

//1. STORE
const store= createStore(reducer);
console.log("before",store.getState())


//ACTION
store.dispatch({type:"ADD_AGE"})
store.dispatch({type:"SET_VALUE",setValue:10})

//REDUX WITH OBJECT
//ADD
store.dispatch({ type: "ADD_CART", id: '4', payload:{ product: { id: '4', name: 'item D' }, total: 1 }})
store.dispatch({ type: "ADD_CART", id: '4', payload:{ product: { id: '4', name: 'item D' }, total: 1 }})
//console.log("after",store.getState())
//DELETE
store.dispatch({ type: "DELETE_CART", id: '2' })
//console.log("DELETE",store.getState())
//UPDATE
store.dispatch({ type: "ADD_CART", id: '9', payload: { product: { id: '4', name: 'item H' }, total: 1 } })
store.dispatch({ type: "UPDATE_CART", id: '9',method:'PLUS' })
store.dispatch({ type: "UPDATE_CART", id: '9',method:'PLUS' })
store.dispatch({ type: "UPDATE_CART", id: '9',method:'PLUS' })
store.dispatch({ type: "UPDATE_CART", id: '9',method:'PLUS' })
store.dispatch({ type: "UPDATE_CART", id: '9',method:'PLUS' })

store.dispatch({ type: "UPDATE_CART", id: '9',method:'MINUS' })
store.dispatch({ type: "UPDATE_CART", id: '9', method:'MINUS' })
store.dispatch({ type: "UPDATE_CART", id: '9', method:'MINUS' })
store.dispatch({ type: "UPDATE_CART", id: '9', method:'MINUS' })
store.dispatch({ type: "UPDATE_CART", id: '9', method:'MINUS' })
store.dispatch({ type: "UPDATE_CART", id: '9', method:'MINUS' })
console.log("after",store.getState())